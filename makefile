# ----------- #
#  VARIABLES  #
# ----------- #

SRC			:= $(wildcard *.c)
BIN			:= $(SRC:%.c=%)
BASE_SRC	:= $(wildcard base/*.c)
BASE_BIN	:= $(BASE_SRC:%.c=%)
CFLAGS		:= -O2


.PHONY: all base clean

all: $(BIN)

base: $(BASE_BIN)

# allow the source code file to be
# specified instead of the output
%.c: | %

%: %.c
	gcc $(CFLAGS) -o $@ $^ $($(notdir $@))

clean:
	rm -rf $(BIN) $(BASE_BIN)


# -------------------------- #
#  PROGRAM-SPECIFIC OPTIONS  #
# -------------------------- #

x86disassembler	:= -lxed
pong			:= -lcurses
