#include <stdio.h>

char digit[] = "0123456789abcdef";

int main() {
    char c, n = -1;
    while ((c = getchar()) != EOF) {
        if (++n == 0)
            printf("{0x");
        else if (n % 4 == 0)
            printf(", 0x");
        putchar(digit[c >> 4]);
        putchar(digit[c & 0xf]);
    }
    while (++n % 4 != 0)
        printf("00");
    puts("}");
    return 0;
}
