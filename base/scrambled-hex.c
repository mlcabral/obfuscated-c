#include <stdio.h>
#include <string.h>

const char digit[] = "0123456789abcdef";

int main(int argc, const char *argv[]) {
    FILE *fp;
    if (argc < 2)
        fp = stdin;

    if (strcmp(argv[1], "-h") == 0) {
        printf("Usage:  %s [-h|<input file>]\n", argv[0]);
        return 0;
    }

    fp = fopen(argv[1], "r");
    if (!fp) {
        printf("Unable to open %s!\n", argv[1]);
        return 1;
    }

    unsigned offset = 0;
    int c = getc(fp);
    char buf[17] = {0};

    while (!feof(fp)) {
        if (offset % 16 == 0)
            // print offset of first byte
            printf("%08x  ", offset);

        printf("%c%c", digit[c >> 4], digit[c & 0xf]);

        if (offset % 2 == 1)
            printf(" ");

        if (c >= 32 && c < 0x7f)
            buf[offset % 16] = c;
        else
            buf[offset % 16] = '.';

        if (offset % 16 == 15)
            printf(" %s\n", buf);

        offset++;
        c = getc(fp);
    }

    int n = offset % 16;
    int n_spaces = (16 - n) * 2.5 + 0.5;
    buf[n] = 0;
    if (n)
        printf(" %*c%s\n", n_spaces, ' ', buf);

    fclose(fp);

    return 0;
}
