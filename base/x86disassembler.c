#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xed/xed-interface.h>

#define ELF_MAGIC_NUMBER    0x464c457f  // "FLE" 0x7f fuck endianness btw
#define MODE_32BIT          1
#define MODE_64BIT          2
#define ISA_X86             0x03
#define ISA_AMD64           0x3e

#define ASSERT(expr)        do { if (!(expr)) { fprintf(stderr, "Assert failed: (" #expr ")\n"); exit(1); } } while (0)
#define READ(addr, n, fp)   ASSERT(fread((addr), (n), 1, (fp)))
#define ADVANCE(fp, n)      ASSERT(!fseek((fp), (n), SEEK_CUR))
#define GOTO(fp, offset)    ASSERT(!fseek((fp), (offset), SEEK_SET))

int isELF(FILE *fp) {
    unsigned buf;
    return fread(&buf, sizeof(unsigned), 1, fp) && buf == ELF_MAGIC_NUMBER;
}

void seekDotText(FILE *fp, long *dotTextSize, int *mode) {
    // assumes the magic number has already been read
    *mode = fgetc(fp);
    ASSERT(*mode == MODE_32BIT || *mode == MODE_64BIT);

    ADVANCE(fp, 13);
    short isa;
    READ(&isa, 2, fp);
    ASSERT(isa == ISA_X86 || isa == ISA_AMD64);

    ADVANCE(fp, 4 + *mode * 8);
    long sectionHeader;
    READ(&sectionHeader, *mode * 4, fp);
#define FIX_32BIT(n)    do { if (*mode == MODE_32BIT) n >>= 4; } while (0)
    FIX_32BIT(sectionHeader);

    ADVANCE(fp, 10);
    short sectionHeaderEntrySize, sectionHeaderEntryNum, sectionNamesIdx;
    READ(&sectionHeaderEntrySize, 2, fp);
    READ(&sectionHeaderEntryNum, 2, fp);
    READ(&sectionNamesIdx, 2, fp);

    // find .text
    long sectionNamesSectionHeader = sectionHeader + sectionNamesIdx * sectionHeaderEntrySize;
    GOTO(fp, sectionNamesSectionHeader + 8 + *mode * 8);

    long sectionNames;
    READ(&sectionNames, *mode * 4, fp);
    FIX_32BIT(sectionNames);

    long dotTextOffset = 0;
    for (short i = 0; i < sectionHeaderEntryNum; i++) {
        if (i == sectionNamesIdx)
            continue;

        long sectionOffset = sectionHeader + i * sectionHeaderEntrySize;
        GOTO(fp, sectionOffset);

        int nameIdx;
        READ(&nameIdx, 4, fp);

        GOTO(fp, sectionNames + nameIdx);

        char buf[6] = {0};
        READ(buf, 5, fp);

        if (strcmp(buf, ".text") == 0) {
            GOTO(fp, sectionOffset + 8 + *mode * 8);

            READ(&dotTextOffset, *mode * 4, fp);
            FIX_32BIT(dotTextOffset);

            READ(dotTextSize, *mode * 4, fp);
            FIX_32BIT(*dotTextSize);
            break;
        }
    }
#undef FIX_32BIT
    ASSERT(dotTextOffset != 0);
    GOTO(fp, dotTextOffset);

    *mode--;
}

void decode(FILE *fp, long length, xed_bool_t longMode) {
    xed_machine_mode_enum_t machineMode;
    xed_address_width_enum_t stackAddrWidth;
    xed_decoded_inst_t xedd;
    char buf[99];
    unsigned char instrBuf[XED_MAX_INSTRUCTION_BYTES];

    xed_tables_init();

    if (longMode) {
        machineMode = XED_MACHINE_MODE_LONG_64;
        stackAddrWidth = XED_ADDRESS_WIDTH_64b;
    } else {
        machineMode = XED_MACHINE_MODE_LEGACY_32;
        stackAddrWidth = XED_ADDRESS_WIDTH_32b;
    }

    int bytesRead = 0;
    long offset = ftell(fp);
    while (!feof(fp) && length-- > 0) {
        // must initialize instruction buffer before every decoding attempt
        xed_decoded_inst_zero(&xedd);
        xed_decoded_inst_set_mode(&xedd, machineMode, stackAddrWidth);

        fread(&instrBuf[bytesRead++], sizeof(char), 1, fp);
        if (xed_decode(&xedd, XED_STATIC_CAST(const xed_uint8_t *, instrBuf), bytesRead) == XED_ERROR_NONE) {
            if (xed_format_context(XED_SYNTAX_INTEL, &xedd, buf, 99, 0, 0, 0)) {
                printf("% 8lx:\t%s\n", offset, buf);
                bytesRead = 0;
                offset = ftell(fp);
            }
        }

        ASSERT(bytesRead <= 15);
    }
}

int main(int argc, const char **argv) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <x86 binary>\n", argv[0]);
        exit(1);
    }

    FILE *fp;
    ASSERT((fp = fopen(argv[1], "r")));

    long size;
    int longMode;
    ASSERT(isELF(fp));
    seekDotText(fp, &size, &longMode);

    printf("<.text> @ 0x%lx:\n", ftell(fp));

    decode(fp, size, longMode);

    fclose(fp);

    return 0;
}
